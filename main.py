# -*- coding: utf8 -*-
from flask import Flask, render_template, request, flash
import MySQLdb

app = Flask(__name__)
#Наша база данных, если MySql установлен локально писать localhost
db = MySQLdb.connect("192.168.99.100", "root", "", "guestbook")

@app.route("/", methods=['POST', 'GET'])
def main():
    error = ''

    if request.method == 'POST':
        name = request.form['name']
        message = request.form['message']

        if not name or not message:
            error = u'Пустое сообщение!'
        else:
            cursor = db.cursor()
            cursor.execute('INSERT INTO posts(name, message) VALUES ("%s", "%s")' % (name, message))
            db.commit()

    #Мы хотим получать словарь вместо списка, потому указываем в курсоре это
    cursor = db.cursor(MySQLdb.cursors.DictCursor)
    cursor.execute('SELECT * FROM posts ORDER BY id DESC')
    posts = cursor.fetchall()

    return render_template('index.html', posts=posts, error=error)

if __name__ == "__main__":
    app.run()