CREATE TABLE `guestbook`.`posts` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `message` TEXT NOT NULL,
  `created` DATETIME NOT NULL DEFAULT NOW(),
  PRIMARY KEY (`id`)
);