git init - для создания репозитория
git status - чтобы увидеть что изменилось
.gitignore - файл со списком файлов и директорий которые будем игнорировать
git add - добавить в файлы в индекс. Т.е то что будем сохранять
git commit - сохранить


virtualenv env - создает папку env и загружает в нее python и pip
source env/bin/activate - активирует виртуальное окружение в папке env/
pip install flask - устанавливает модуль flask для _python_

mysql
	insert - добавления новых записей #INSERT INTO posts(name, message) VALUES("magiq", "hello world")
	select - для получения записей #select name, message from posts;
	update - для редактирования старых 
	delete - для удаления #DELETE FROM posts WHERE id=1;
	create database posts - для добавления

	CREATE TABLE posts(
		id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, #AUTO_INCREMENT обязательно primary key
		name VARCHAR(45) NOT NULL,
		message VARCHAR(400) NOT NULL
	);

В хттп всего 2 метода передачи данных GET, POST